'use strict';

var request   = require('supertest');
var chai      = require('chai');
var restify   = require('restify');
var expect    = chai.expect;
var url = 'http://127.0.0.1:8080';

describe('Carts Api Test', () => {
    var api;
    var server;
    beforeEach(() => {
        server = restify.createServer();
        server.use(restify.acceptParser(server.acceptable));
        server.use(restify.queryParser());
        server.use(restify.bodyParser());

        server.listen(8080, () =>  { console.log("server started");});
        var api = require('./server.js').init(server);
    });


    afterEach( () => {
        server.close();
    });

    it('checkout should do a checkout', (done) => {
        var app = request(url);

        var testCart  = { cartId:{UID:1},
                          userId:{UID:10009},
                          emailAddress:"testuser@example.com",
                          billingAddressId:{UID:1},
                          paymentService:"Stripe",
                          paymentToken:"payement_token_xxxxx" };

        app.post("/v1/cart/checkout")
            .send(testCart)
            .expect(200, done);

    });


    it("should get the current user's cart", (done) => {
        var app = request(url);
        app.post("/v1/cart")
            .send({userId: {UID: 10009}})
            .expect(200, done);
    });

    it("should respond with unauthorized with invalid user", (done) => {
        var app = request(url);
        app.post("/v1/cart")
            .send({})
            .expect(401, done);
    });

    it("should respond with unauthorized with invalid cart", (done) => {
        var app = request(url);
        app.post("/v1/cart/items/remove")
            .send({})
            .expect(400, done);
    });

    it("should respond with unauthorized with invalid cart", (done) => {
        var app = request(url);
        app.post("/v1/cart/items/update")
            .send({})
            .expect(400, done);
    });

    it("should remove items", (done) => {
        var app = request(url);
        app.post("/v1/cart/items/remove")
            .send({ cartId:{UID:1}, items: [ {UID: 7 }, {UID: 8 } ]})
            .expect(200, done);
    });

    it("should add items to cart", (done) => {
        var app = request(url);

        var items = { cartId:{UID:16},
                      items: [ { productId: {UID:14},
                                 purchasePrice: 1,
                                 quantity: 1 } ] };
        app.post("/v1/cart/items/add")
            .send(items)
            .expect(200, done);
    });


    it("should update items ", (done) => {
        var app = request(url);

        var items = { cartId:{UID:16},
                      items: [ { productId: {UID:14},
                                 purchasePrice: 1,
                                 quantity: 1 } ] };
        app.post("/v1/cart/items/update")
            .send(items)
            .expect(200, done);
    });

});
