var restify = require('restify');


var init = (por) => {
    // Server
    var server = restify.createServer({
        name: 'carter',
        version: '1.0.0'
    });

    var cartApi = require('./server.js');

    cartApi.init(server);
    server.use(restify.acceptParser(server.acceptable));
    server.use(restify.queryParser());
    server.use(restify.bodyParser());

    server.listen(8080, () => {
        console.log('%s listening at %s', server.name, server.url);
    });

};

init();
