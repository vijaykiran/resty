var restify = require('restify');
var _       = require('lodash');

var getCart = (req, res, next) => {
    var user  = req.body;
    if(user.userId || user.username) {
        res.send(200, _.merge(user, {cartId: {UID: 1}}));
    } else {
        res.send(401);
    }
    next();
};

var addItems = (req, res, next) => {
    var cart  = req.body;
    if(cart.items) {
        console.log("add items to cart");
        res.send(200);
    } else {
        res.send(400);
    }
    next();
};

var checkoutCart = (req, res, next) => {
    var cart = req.body;
    var requiredKeys = ['cartId', 'userId', 'emailAddress',
                        'billingAddressId', 'paymentService',
                        'paymentToken'];
    console.log("cart is : " + cart);
    if(_.isEqual(requiredKeys.sort(), _.keys(cart).sort())) {
        console.log("Initiate payment");
        res.send(200);
    } else {
        res.send(400);
    }
    next();
};

var removeItems = (req, res, next) => {
    var cart = req.body;
    if(cart.items) {
        console.log("remove items from cart");
        res.send(200);
    } else {
        res.send(400);
    }
    next();
};

var updateItems = (req, res, next) => {
    var cart = req.body;
    if(cart.items) {
        console.log("update items");
        res.send(200);
    } else {
        res.send(400);
    }
    next();
};

exports.init = (server) => {
    server.post('/v1/cart', getCart);
    server.post('/v1/cart/items/remove', removeItems);
    server.post('/v1/cart/items/add', addItems);
    server.post('/v1/cart/items/update', updateItems);
    server.post('/v1/cart/checkout', checkoutCart);
};
